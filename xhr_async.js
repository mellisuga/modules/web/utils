'use strict'


const BSON = require("bson");
const XHR = require("core/utils/xhr_async");

module.exports = class {
  static async get(url, params, flyauth) {
    try {
      console.log("GET", url);
      return await new Promise(function (resolve) {
        if (params) {
          url += "?data="+encodeURIComponent(JSON.stringify(params));
        }

        var xhr = new XMLHttpRequest();

        xhr.addEventListener("load", function() {
          try {
            if (this.responseText.startsWith("<svg")) {
              resolve(this.responseText);
            } else {
              try { 
                resolve(JSON.parse(this.responseText));
              } catch (e) {
                resolve(this.responseText);
              }
            }
          } catch (e) {
            console.error(e.stack, "this.responseText:", this.responseText);
          }
        });
        xhr.open("GET", url);
        xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        if (typeof AUTHENTIALS !== 'undefined') {

          console.log("AUTHENTIALS", AUTHENTIALS);
          xhr.setRequestHeader("Authorization", AUTHENTIALS);
        }
        xhr.send();
      });
    } catch(e) {
      console.error(e.stack);
      return undefined;
    }
  }

  static async get_bson(url, params, flyauth) {
    try {
      return await new Promise(function (resolve) {
        if (params) {
          url += "?data="+encodeURIComponent(JSON.stringify(params));
        }

        var xhr = new XMLHttpRequest();
        xhr.responseType = "arraybuffer";


        xhr.addEventListener("load", function() {
          resolve(BSON.deserialize(new Uint8Array(this.response)));
        });
        xhr.open("GET", url);
        if (typeof AUTHENTIALS !== 'undefined') {
          xhr.setRequestHeader("Authorization", AUTHENTIALS);
        }
        xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        xhr.send();
      });
    } catch(e) {
      console.error(e);
      return undefined;
    }
  }

  static async post(url, params, flyauth) {
    try {
      return await new Promise(function (resolve) {
        if (params.formData) {
          var xhr = new XMLHttpRequest();

          xhr.addEventListener("load", function() {
            try {
              resolve(JSON.parse(xhr.responseText));
            } catch (e) {
              resolve(xhr.responseText);
            }
          });

          if (params.on_progress) {
            
            xhr.upload.addEventListener('progress', params.on_progress);

          }
          xhr.open("POST", url);
        //  xhr.setRequestHeader("Content-Type","multipart/form-data");
          xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
          

          if (typeof AUTHENTIALS !== 'undefined') {
            xhr.setRequestHeader("Authorization", AUTHENTIALS);
          }

          if (params.headers) {
            xhr.setRequestHeader("params", encodeURIComponent(JSON.stringify(params.headers)));
          }

          xhr.send(params.formData);

        } else {
          var xhr = new XMLHttpRequest();
          xhr.onreadystatechange = function() {//Call a function when the state changes.
            if(xhr.readyState == 4 && xhr.status == 200) {
              try {
                resolve(JSON.parse(xhr.responseText));
              } catch (e) {
                resolve(xhr.responseText);
              }
            } else if (xhr.readyState == 4 && xhr.status == 401)  {
              resolve("UNAUTHORIZED");
            }
          }
          xhr.open("POST", url, true);

          //Send the proper header information along with the request
          xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");


          xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");

          
          if (typeof AUTHENTIALS !== 'undefined') {
            console.log("SET AUTHENTIALS", AUTHENTIALS);
            xhr.setRequestHeader("Authorization", AUTHENTIALS);
          }


          var json = "";
          if (typeof params == "string") {
            json = params;
          } else {
            json = JSON.stringify(params);
          }
          console.log("PARAMS", json);
          var param_str = 'data='+encodeURIComponent(json);
      
          console.log("POST:", param_str);
          xhr.send(param_str); 
        }
      });
    } catch(e) {
      console.error(e);
      return undefined;
    }
  }

  static getParamByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
  }

  static async getText(url, params, flyauth) {
    try {
      console.log("GET TEXT", url);
      return await new Promise(function (resolve) {
        if (params) {
          url += "?data="+encodeURIComponent(JSON.stringify(params));
        }

        var xhr = new XMLHttpRequest();

        xhr.addEventListener("load", function() {
          try {
            resolve(this.responseText);
          } catch (e) {
            console.error(e.stack, "this.responseText:", this.responseText);
          }
        });
        xhr.open("GET", url);
        xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        if (typeof AUTHENTIALS !== 'undefined') {
          xhr.setRequestHeader("Authorization", AUTHENTIALS);
        }
        xhr.send();
      });
    } catch(e) {
      console.error(e);
      return undefined;
    }
  }

}
