

module.exports = class {
  static async construct() {
    try {
      let _this = new module.exports();
      
      return _this;
    } catch (e) {
      console.error(e.stack);
    }
  }

  static yyyy_mm_dd(idate, jchar) {
    if (!jchar) jchar = "-";
    let udate = new Date(parseInt(idate));
    let month = udate.getMonth()+1;
    if (month.toString().length == 1) month = "0"+month;

    let day = udate.getDate();
    if (day.toString().length == 1) day = "0"+day;
    return udate.getFullYear()+jchar+month+jchar+day;
  }
}
